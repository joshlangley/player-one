import playerone.*;

public class Main {
    public static void main(String[] args) {
        int runs;
        int gameCount = 0;
        
        // Get the number of runs from the first argument.
        try {
            runs = Integer.parseInt(args[0]);
            if (runs < 0) { // If runs is negative, invert it and tell the user.
                runs *= -1;
                System.out.println("\nWARNING: Invalid number of runs. (Must be positive.) Using "+runs+".\n");
            } else if (runs == 0) { // If runs is 0, use 1 and tell the user.
                runs = 1;
                System.out.println("\nWARNING: Invalid number of runs. (Must be non-zero.) Using "+runs+".\n");
            }
        } catch(Exception e) { // If no args specified or other error, use 1 and tell the user.
            runs = 1;
            System.out.println("\nWARNING: Invalid number of runs. Using "+runs+".\n");
        }

        // Get the level of verbosity from the second argument.
        try {
            int verbosityCode = Integer.parseInt(args[1]);
            if (verbosityCode < 0) { // If runs is negative, invert it and tell the user.
                verbosityCode *= -1;
                System.out.println("\nWARNING: Invalid verbosity code. (Must be positive.) Using "+verbosityCode+".\n");
            } else if (verbosityCode == 0) { // If runs is 0, use 1 and tell the user.
                verbosityCode = 1;
                System.out.println("\nWARNING: Invalid verbosity code. (Must be non-zero.) Using "+verbosityCode+".\n");
            }
            PrintStatus.parseCode(verbosityCode);
        } catch(Exception e) { // If no args specified or other error, use 1 and tell the user.
            int verbosityCode = 1;
            System.out.println("\nWARNING: Invalid verbosity code. Using "+verbosityCode+".\n");
            PrintStatus.parseCode(verbosityCode);
        }

        // Get the number of milliseconds to pause between operations from the third argument.
        try {
            int delay = Integer.parseInt(args[2]);
            if (delay < 0) { // If runs is negative, invert it and tell the user.
                delay *= -1;
                System.out.println("\nWARNING: Invalid delay length. (Must be positive.) Using "+delay+".\n");
            }
            Game.RUN_DELAY_MILLISECONDS = delay;
        } catch(Exception e) { // If no args specified or other error, use 1 and tell the user.
            int delay = 0;
            System.out.println("\nWARNING: Invalid delay length. Using "+delay+".\n");
            Game.RUN_DELAY_MILLISECONDS = delay;
        }

        // Populate the wins table for all players
        for (Player player : PlayerList.getPlayerList()) Game.wins.put(player.getName(), 0);
        
        // Game Governer
        while (gameCount < runs) {
            Game game = new Game(gameCount);
            game.run(); // Singlethreaded
            gameCount++;
            // game.start(); // Multithreaded
        }

        while (Game.finishedGameCount < gameCount) {
            System.out.println("Waiting for all games to complete...");
            // for (Game activeGame : gamelist) System.out.println("    Game "+activeGame.getGameNumber()+" still running.");
            try {
                Thread.sleep(20);
            } catch(Exception e) {
                System.out.println("\nERROR waiting for games to complete!\n");
            }
        }

        for (String winnerName : Game.winners) {
            try {
                Game.wins.put(winnerName, Game.wins.get(winnerName)+1);
            } catch(NullPointerException n) {
                // If the winner has no previous entry, create one.
                Game.wins.put(winnerName, 1);
            }
        }

        PrintStatus.notifySimulationResults();

    }
}
