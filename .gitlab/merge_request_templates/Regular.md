# (Merge Request Title)

Closes <#IssueNum>

# What?
What does this merge request do?

# Why?
Why do we need this merge request?

# How?
Provide a brief explanation of what you did.
This should ideally provide context and make it easier for a reviewer to read how and why you did things a certain way.

# Testing?
What testing did you do?
How thoroughly is your code tested?

# Screenshots (optional)
If you changed anything that is better explained in pictures, include those here.

# Reviewer Tasks
The following tasks should be completed by a reviewer prior to this code getting merged in.
If you would like a reviewer to test any of your code, explain **in detail** all the steps necessary to test the code.
Only one reviewer must complete these tasks.
Once a reviewer has completed these tasks, they should mark the checkbox.

* [ ] item 1
* [ ] item 2
* [ ] item 3

# Review Checklist
The following sections detail some items that reviewers should think about and mark the checkbox for when they agree.
The programmers who are listed below are the ones responsible for reviewing this merge request.
Before this merge request gets merged in, each reviewer should complete a thorough review of the code and mark all the checkboxes as completed.

## (Reviewer Name)
* [ ] This merge request benefits the overall health of the codebase
* [ ] The code has been tested appropriately
* [ ] The commit messages have been reviewed and conform to the git standards.
* [ ] Comments in the code are clear and useful, and mostly explain **why** instead of what
* [ ] Every line of code I was assigned to review has been reviewed
* [ ] The context of the changes has been taken into consideration. These code changes still benefit the overall health of the codebase.
* [ ] The merge request looks good to me and should be merged

## (Reviewer Name)
* [ ] This merge request benefits the overall health of the codebase
* [ ] The code has been tested appropriately
* [ ] The commit messages have been reviewed and conform to the git standards.
* [ ] Comments in the code are clear and useful, and mostly explain **why** instead of what
* [ ] Every line of code I was assigned to review has been reviewed
* [ ] The context of the changes has been taken into consideration. These code changes still benefit the overall health of the codebase.
* [ ] The merge request looks good to me and should be merged
