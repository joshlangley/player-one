package playerone;

import java.util.ArrayList;

public class PrintStatus {
    // Verbosity level - how much detail to print to the user
    // Uses UNIX-style octal codes
    // simulator = 1 check for s: verbosityLevel%2 != 0
    // governor = 2 check for g: (verbosityLevel/2)%2 != 0
    // player = 4 check for p: (verbosityLevel/4)%2 != 0
    public static int verbosityLevel = 1;

    public static int simulatorID = 1;
    public static int governorID = 2;
    public static int playerID = 4;

    private static boolean simulator;
    private static boolean governor;
    private static boolean player;

    public static void parseCode(int code) {
        verbosityLevel = code;

        simulator = (code%2 != 0);
        governor = ((code/governorID)%2 != 0);
        player = ((code/playerID)%2 != 0);
    }

    // General
    

    // Simulation methods
    public static void notifySimulationResults() {
        if (simulator) {
            ArrayList<Player> players = PlayerList.getPlayerList();
            ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
            ArrayList<String> row;

            String padding = " "; // The string to pad each table element with
            char colDivider = '│'; // The character to use for the table column divider
            char headerDivider = '━'; // The character to use for the table header divider
            char intersectionDivider = '┿'; // The character to use as the divider at the intersection of the header and column dividers
            int totalGames = 0;
            for (Player player : players) totalGames += Game.wins.get(player.getName()); // Count the total number of games played

            // Populate the header row
            row = new ArrayList<String>();
            row.add("Name");
            row.add("Games Won");
            row.add("Percent Won");
            table.add(row);

            // Add a header divider row flag
            row = new ArrayList<String>();
            row.add("divider");
            table.add(row);

            // Populate the content
            for (Player player : players) {
                row = new ArrayList<String>();

                String name = player.getName();
                int wins = Game.wins.get(name);

                row.add(name);
                row.add(Integer.toString(wins));
                row.add(Integer.toString( (int) (100 * ((double) wins/totalGames)) )+"%");

                table.add(row);
            }

            // Find each column's size (without padding) by finding the longest element in that column
            int[] colSizes = new int[table.get(0).size()];
            for (int rowNum = 0; rowNum < colSizes.length; rowNum++) {
                for (int colNum = 0; colNum < table.size(); colNum++) {
                    try {
                        int entryLength = table.get(colNum).get(rowNum).length();
                        if (entryLength > colSizes[rowNum]) colSizes[rowNum] = entryLength;
                    } catch (Exception e) {} // Ignore dividers and other silly rows
                }
            }

            System.out.println();
            // Print the table
            for (int rowNum = 0; rowNum < table.size(); rowNum++) {
                ArrayList<String> tableRow = table.get(rowNum);
                String outputRow = "";

                if (tableRow.get(0).equals("divider")) { // If a divider row (like the header), build output differently

                    outputRow += intersectionDivider; // Start the row with an intersection divider
                    for (int colNum = 0; colNum < table.get(0).size(); colNum++) { // Iterate however many columns are in the header row
                        String divider = ""; // The divider between the column header and content
                        int targetWidth = colSizes[colNum] + 2*padding.length();          // Calculate the total width of the column (element plus padding)
                        while (divider.length() < targetWidth) divider += headerDivider; // Append that many header dividers
                        outputRow += divider + intersectionDivider;                     // Append that column's header divider plus another intersection divider
                    }

                } else { // If header or content row, build normally

                    outputRow += colDivider; // Start the row with a column divider
                    for (int colNum = 0; colNum < tableRow.size(); colNum++) {
                        String element = tableRow.get(colNum);
                        while (element.length() < colSizes[colNum]) element += " "; // Fill in the element's full size
                        outputRow += padding + element + padding; // Add the padded element
                        outputRow += colDivider; // Append a column divider
                    }
                }

                System.out.println(outputRow);
            }
            System.out.println();
        }
    }

    public static void notifySimulatorCustom(String message) {
        if (simulator)
            System.out.println(message);
    }

    // Governor methods
    public static void notifyGameStart(int gameNum) {
        if (governor)
            System.out.println("Starting game "+gameNum);
    }

    public static void notifyGameCompletion(int gameNum) {
        if (governor)
            System.out.println("Completed game "+gameNum+"\n"); // +" on thread "+threadNum+".");
    }

    public static void notifyGameWinner(String winnerName) {
        if (governor)
            System.out.println(winnerName+" has won!");
    }

    public static void printPlayerTransactionBuffer() {
        if (player)
            System.out.println();
    }

    public static void notifyGovernorCustom(String message) {
        if (governor)
            System.out.println(message);
    }

    // Player methods
    public static void notifyDed(Player deadPlayer) {
        if (player)
            System.out.println("    Player "+deadPlayer.getName()+" has died!");
    }

    public static void notifyCriticalAttackSuccessful(Player attacker, Player defendant) {
        if (player)
            System.out.println("    "+attacker.getName()+" is critically attacking "+defendant.getName()+" with power of "+attacker.getCriticalHitPower()+"!");
    }

    public static void notifyAttackSuccessful(Player attacker, Player defendant) {
        if (player)
            System.out.println("    "+attacker.getName()+" is attacking "+defendant.getName()+" with power of "+attacker.getAttackPower()+"!");
    }

    public static void notifyAttackFailed(Player attacker, Player defendant) {
        if (player)
            System.out.println("    "+attacker.getName()+" missed an attack on "+defendant.getName()+"!");
    }

    public static void notifyBlockSuccessful(Player defender, Player attacker, int attackPower) {
        if (player && attackPower != 0)
            System.out.println("    "+defender.getName()+" blocked an attack of "+attackPower+" from "+attacker.getName()+"!");
    }

    public static void notifyBlockPartway(Player defender, Player attacker, int attackPower) {
        if (player && attackPower != 0)
            System.out.println("    "+defender.getName()+" partially blocked an attack of "+attackPower+" from "+attacker.getName()+"!");
    }

    public static void notifyBlockFailed(Player defender, Player attacker, int attackPower) {
        if (player && attackPower != 0)
            System.out.println("    "+defender.getName()+" took a hit of "+attackPower+" from "+attacker.getName()+"!");
    }

    public static void notifyPlayerCustom(String message) {
        if (player)
            System.out.println("    "+message);
    }

}
