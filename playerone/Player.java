package playerone;

import java.util.ArrayList;

public abstract class Player {

    private String name;
    private int health;

    private int attackProb;
    private int attackPower;
    private int criticalHitProb;
    private int criticalHitPower;
    private int sheildProb;

    public Player(String playerName, int playerHealth, int playerAttackProb, int playerAttackPower, int playerCriticalHitProb, int playerCriticalHitPower, int playerShieldProb) {
        name = playerName;
        health = playerHealth;
        attackProb = playerAttackProb;
        attackPower = playerAttackPower;
        criticalHitProb = playerCriticalHitProb;
        criticalHitPower = playerCriticalHitPower;
        sheildProb = playerShieldProb;
    }

    public int attack(Player defendant) {
        int roll = (int)(Math.random()*100);

        Game.delay();

        if (roll < criticalHitProb) {
            PrintStatus.notifyCriticalAttackSuccessful(this, defendant);
            return criticalHitPower;
        } else if (roll < attackProb) {
            PrintStatus.notifyAttackSuccessful(this, defendant);
            return attackPower;
        } else {
            PrintStatus.notifyAttackFailed(this, defendant);
            return 0;
        }
    }

    public void block(int power, Player attacker) {
        Game.delay(0.5);

        PrintStatus.notifyBlockFailed(this, attacker, power);
        incrementHealth(-power);
    }

    public Player chooseDefendant(ArrayList<Player> players) {
        // Player defendant = players.get(die.nextInt(players.size()));
        Player defendant = players.get((int)(Math.random()*players.size()));

        while (defendant == this) {
            // defendant = players.get(die.nextInt(players.size()));
            defendant = players.get((int)(Math.random()*players.size()));
        }

        return defendant;        
    }

    public void incrementHealth(int delta) {
        health+=delta;
    }

    // Getters

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    // Stats Getters
    public int getAttackProb() {
        return attackProb;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public int getCriticalHitProb() {
        return criticalHitProb;
    }

    public int getCriticalHitPower() {
        return criticalHitPower;
    }

    public int getShieldProb() {
        return sheildProb;
    }

    // Special Getters
    public String getStatistics() {
        String msg = "";
        msg += this.name + "\n";
        msg += "Starting Health: " + this.getHealth() + "\n";
        msg += "Attack Prob: " + this.getAttackProb()  + "\n";
        msg += "Attack Power: " + this.getAttackPower() + "\n";
        msg += "Critical Hit Prob: " + this.getCriticalHitProb() + "\n";
        msg += "Critical Hit Power: " + this.getCriticalHitPower() + "\n";
        msg += "Shield Prob: " + this.getShieldProb();
        return msg;
    }

    public String toString() {
        return this.name;
    }
}
