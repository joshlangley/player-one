package playerone;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Game extends Thread {
    public static ArrayList<String> winners = new ArrayList<String>();
    public static Lock winnersLock = new ReentrantLock();

    public static Hashtable<String, Integer> wins = new Hashtable<String, Integer>();

    public static int finishedGameCount = 0;

    public static int RUN_DELAY_MILLISECONDS = 0;

    private Random die = new Random();

    private ArrayList<Player> players;
    private int gameNumber;

    public Game(int gameNumber) {
        this.gameNumber = gameNumber;

        players = PlayerList.getPlayerList();
    }

    public Player getAttacker() {
        return players.get(die.nextInt(players.size()));
    }

    public void cleanPlayers() {
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getHealth() <= 0) {
                PrintStatus.notifyDed(players.get(i));
                players.remove(i);
                i--;
            }
        }
    }

    public void printHealths() {
        System.out.println("\nPLAYER HEALTHS\n================");
        int nameColLength = getLongestName()+1;

        for (Player player : players) {
            String nameCol = player.getName();
            while (nameCol.length() < nameColLength) nameCol += " ";

            String healthCol = "";
            while (healthCol.length() < player.getHealth()) healthCol += "=";
            healthCol += " "+player.getHealth();

            System.out.println(nameCol + healthCol);
        }
    }

    public int getLongestName() {
        int longestNameLength = 0;
        int nameLength;

        for (Player player : players) {
            nameLength = player.getName().length();
            if (nameLength > longestNameLength) longestNameLength = nameLength;
        }

        return longestNameLength;
    }

    public void run() {
        Player attacker;
        Player defendant;

        PrintStatus.notifyGameStart(gameNumber);

        while (players.size() > 1) {
            attacker = getAttacker();
            defendant = attacker.chooseDefendant(players);

            int attackPower = attacker.attack(defendant);
            defendant.block(attackPower, attacker);

            cleanPlayers();
            PrintStatus.printPlayerTransactionBuffer();
        }

        String winnerName = players.get(0).getName();

        winnersLock.lock();
        winners.add(winnerName);
        winnersLock.unlock();

        PrintStatus.notifyGameWinner(winnerName);
        PrintStatus.notifyGameCompletion(gameNumber);
        finishedGameCount++;

        delay();
    }

    public static void delay() {
        delay(1.0);
    }

    public static void delay(double proportion) {
        try {
            Thread.sleep((int) (proportion*RUN_DELAY_MILLISECONDS));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getGameNumber() {
        return gameNumber;
    }
}
