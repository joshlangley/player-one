package playerone.playersets.lord_of_the_rings;

import playerone.*;

public class RohanMan extends Man {

    private static final int INIT_HEALTH = 30;
    private static final int ATTACK_PROB = 83;
    private static final int ATTACK_POWER = 4;
    private static final int CRITICAL_HIT_PROB = 0;
    private static final int CRITICAL_HIT_POWER = 0;
    private static final int SHIELD_PROB = 17;
    private static final int HALF_SHIELD_PROB = 33;

    public RohanMan(String playerName) {
        super(playerName, INIT_HEALTH, ATTACK_PROB, ATTACK_POWER, CRITICAL_HIT_PROB, CRITICAL_HIT_POWER, SHIELD_PROB);
    }

    public void block(int power, Player attacker) {
        Game.delay(0.5);
        // int roll = getDie().nextInt(100);
        int roll = (int)(Math.random()*100);
        
        if (roll < SHIELD_PROB) {
            PrintStatus.notifyBlockSuccessful(this, attacker, power);
        } else if (roll < HALF_SHIELD_PROB) {
            PrintStatus.notifyBlockPartway(this, attacker, power);
            incrementHealth(-(int)(power/2));
        } else {
            PrintStatus.notifyBlockFailed(this, attacker, power);
            incrementHealth(-power);
        }

    }
}
