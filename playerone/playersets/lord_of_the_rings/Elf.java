package playerone.playersets.lord_of_the_rings;

import playerone.*;

public class Elf extends Player {

    private static final int INIT_HEALTH = 45;
    private static final int ATTACK_PROB = 83;
    private static final int ATTACK_POWER = 4;
    private static final int CRITICAL_HIT_PROB = 17;
    private static final int CRITICAL_HIT_POWER = 8;
    private static final int SHIELD_PROB = 0;

    public Elf(String playerName) {
        super(playerName, INIT_HEALTH, ATTACK_PROB, ATTACK_POWER, CRITICAL_HIT_PROB, CRITICAL_HIT_POWER, SHIELD_PROB);
    }    
}
