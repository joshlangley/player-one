package playerone.playersets.lord_of_the_rings;

public class GondorMan extends Man {

    private static final int INIT_HEALTH = 40;
    private static final int ATTACK_PROB = 83;
    private static final int ATTACK_POWER = 2;
    private static final int CRITICAL_HIT_PROB = 0;
    private static final int CRITICAL_HIT_POWER = 0;
    private static final int SHIELD_PROB = 33;

    public GondorMan(String playerName) {
        super(playerName, INIT_HEALTH, ATTACK_PROB, ATTACK_POWER, CRITICAL_HIT_PROB, CRITICAL_HIT_POWER, SHIELD_PROB);
    }
}
