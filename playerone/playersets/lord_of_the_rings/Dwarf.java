package playerone.playersets.lord_of_the_rings;

import playerone.*;

import java.util.Random;

public class Dwarf extends Player {

    private static final int INIT_HEALTH = 36;
    private static final int ATTACK_PROB = 66;
    private static final int ATTACK_POWER = 4;
    private static final int HIGH_ATTACK_PROB = 33;
    private static final int HIGH_ATTACK_POWER = 7;
    private static final int CRITICAL_HIT_PROB = 17;
    private static final int CRITICAL_HIT_POWER = 12;
    private static final int SHIELD_PROB = 0;

    private Random die = new Random();

    public Dwarf(String playerName) {
        super(playerName, INIT_HEALTH, ATTACK_PROB, ATTACK_POWER, CRITICAL_HIT_PROB, CRITICAL_HIT_POWER, SHIELD_PROB);
    }

    public int attack(Player defendant) {
        int roll = die.nextInt(100);

        Game.delay();

        if (roll < getCriticalHitProb()) {
            PrintStatus.notifyCriticalAttackSuccessful(this, defendant);
            return getCriticalHitPower();
        } else if (roll < HIGH_ATTACK_PROB) {
            PrintStatus.notifyPlayerCustom(getName()+" is high-level attacking "+defendant.getName()+" with power of "+HIGH_ATTACK_POWER+"!");
            return HIGH_ATTACK_POWER;
        } else if (roll < getAttackProb()) {
            PrintStatus.notifyAttackSuccessful(this, defendant);
            return getAttackPower();
        } else {
            PrintStatus.notifyAttackFailed(this, defendant);
            return 0;
        }
    }
}
