package playerone.playersets.lord_of_the_rings;

import playerone.*;

public class Man extends Player {

    private static final int INIT_HEALTH = 33;
    private static final int ATTACK_PROB = 100;
    private static final int ATTACK_POWER = 3;
    private static final int CRITICAL_HIT_PROB = 66;
    private static final int CRITICAL_HIT_POWER = 5;
    private static final int SHIELD_PROB = 17;

    public Man(String playerName) {
        super(playerName, INIT_HEALTH, ATTACK_PROB, ATTACK_POWER, CRITICAL_HIT_PROB, CRITICAL_HIT_POWER, SHIELD_PROB);
    }

    public Man(String playerName, int initHealth, int playerAttackProb, int playerAttackPower, int playerCriticalHitProb, int playerCriticalHitPower, int playerShieldProb) {
        super(playerName, initHealth, playerAttackProb, playerAttackPower, playerCriticalHitProb, playerCriticalHitPower, playerShieldProb);
    }

    public void block(int power, Player attacker) {
        Game.delay(0.5);
        
        if (Math.random()*100 < SHIELD_PROB) {
            PrintStatus.notifyBlockSuccessful(this, attacker, power);
        } else {
            PrintStatus.notifyBlockFailed(this, attacker, power);
            incrementHealth(-power);
        }
    }
}
