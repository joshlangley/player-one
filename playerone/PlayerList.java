package playerone;

import java.util.ArrayList;

import playerone.playersets.lord_of_the_rings.*;

public class PlayerList {
    
    public static ArrayList<Player> getPlayerList() {
        ArrayList<Player> playerList = new ArrayList<Player>();

        // Basic Set
        playerList.add(new Elf("Elf"));
        playerList.add(new Dwarf("Dwarf"));
        playerList.add(new Man("Man"));
        playerList.add(new GondorMan("GondorMan"));
        playerList.add(new RohanMan("RohanMan"));

        // Class Set
        // playerList.add(new Man("Mattew"));
        // playerList.add(new RohanMan("Jacob C"));
        // playerList.add(new RohanMan("Ben"));
        // playerList.add(new Man("Jaivin"));
        // playerList.add(new Elf("Trevor"));
        // playerList.add(new Elf("Jacob J"));
        // playerList.add(new Man("Jason"));
        // playerList.add(new GondorMan("Josh"));
        // playerList.add(new RohanMan("Gabriel"));
        // playerList.add(new Dwarf("Connor"));
        // playerList.add(new RohanMan("Evan"));
        // playerList.add(new GondorMan("Oliver"));
        // playerList.add(new RohanMan("Alex"));
        // playerList.add(new Man("Byron"));
        // playerList.add(new Dwarf("Liam"));
        // playerList.add(new Dwarf("Ethan"));

        return playerList;
    }
}
