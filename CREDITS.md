# Credits

These are the credits for each component of the game. They also highlight special changes from the original version done in class.

## Other components
- Levels of verbosity : Joshua Langley
- Variable delay : Joshua Langley
- Pretty simulation results table : Joshua Langley
