
# Player One
A simulated dice driven PVP game of spontaneous characters and lots of fun.

## Dependencies

This project depends the Java 11+ JDK, ideally via OpenJDK. All library dependencies are included with the JDK.

Additionally, the project optionally uses the VS Code IDE with the "Extension Pack for Java" extension installed for development.

## Running

The code can be complied with `javac Main.java` and run with `java Main [numGames]` where the optional argument `[numGames]` is the number of simulated games to run. `.class` files will need to be cleaned up afterwards.

On Unix-like systems (such as macOS and Linux), this can be done in one step using the wrapper script `./run.sh [numGames]`.

Windows users can also use the "Run Java" option in VS Code, and then use the up arrow in the new terminal panel to pass arguments to it.

## Developing

### CREDITS.md

While developing, it is important that *all* special changes be added to `CREDITS.md` according to the format in issue #3, as this is the list of authors and contributors, as well as the file Mr. Yeh will look at to grade what was done on the project. Additions to `CREDITS.md` should be their own commits.

### Merging

Development of this project is done using Merge Requests. This means all features and bugfixes are introduced via Merge Requests, and the `main` branch is protected and cannot be pushed to. 

All MRs should start with "Draft: " while being worked on, and then "Draft: " should be removed once the code is ready to be reviewed. An approved reviewer will then ensure the code passes all checks:
- Is clean and follows any established convention
- Builds and runs, and is implemented in a clean and concise way
- Any other special tasks specified by the MR author
- Is up-to-date with `main` via a rebase (see below)

If a MR passes all checks, is approved by at least two people (including the reviewer), it will be merged into `main` by the reviewer. No author should review their own MRs, even if they have the power to merge.

### Rebasing

Before merging, the commit base of the MR's branch must be up to date with `main`. This can be done using a simple rebase:

1. Commit and push or stash all changes
2. Run `git fetch` and then `git pull` to be sure the local branch is up to date with the remote
3. Run `git rebase origin/main` to bring the tip of `main` as the commit base for this branch
4. Resolve all conflicts and stage files with `git add <files>`
5. Double check that all conflicts are resolved and staged using `git status`, and then use `git rebase --continue` to finish the rebase
6. Run `git push --force-with-lease` to force update the remote branch with the new commit base

Simple rebases can also be done from the MR in GitLab if no conflicts would be produced. The "Merge" button will instead say "Rebase" and will be active if this is the case.

### Player Sets

New player classes must be included as part of player sets. This is to improve organization and cleanliness within the code. To create a player set, simply create a folder under `playerone/playersets`. This folder should be the name of the player set, containing only lowercase letters and underscores, and it should not start with a number. Player classes can then be created inside this new folder.

Player classes must start with the following in order to compile and run with the rest of the code:

```java
package playerone.playersets.<playerset_folder_name>;

import playerone.*;
```

The first line declares the playerset package the class is a member of, which tells the rest of the code where to find it. The second line imports the `Game`, `Player`, and `PlayerList` classes for use within the class.

Player classes should extend the `Player` class, and override any of the default `Player` methods that do not match the desired behavior of the player. This will primarily include `chooseDefendant(ArrayList<Player>)`, `attack()`, and `block()`.